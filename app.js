import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import Debug from 'debug'; 
import express from 'express';
import logger from 'morgan';
// import favicon from 'serve-favicon';
import path from 'path';
import { middleware as stylusMiddleware } from 'stylus';
import index from './routes/index';
import session from 'express-session';
import passport from 'passport';


const app = express();
const debug = Debug('fullapplication-1:app');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));

app.use(cookieParser());
app.set('trust proxy', 1)

app.use(session({
  secret:"testtest",
  name:"cookie_name",
  cookie: { maxAge: 36000000,
    httpOnly: false},
    saveUninitialized:true

}));
    app.use(passport.initialize());
    app.use(passport.session());


app.use(stylusMiddleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'views')));

app.set('views',path.join(__dirname, 'views'));
app.set('view engine','ejs');
app.use('/', index);



export default app;
