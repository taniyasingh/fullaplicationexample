var app = angular.module('myApp', ["ui.router", "ngResource","ngFileUpload"]);

app.config(function($stateProvider, $urlRouterProvider, $httpProvider)
{

   $stateProvider
     .state("home", {
      url: "/home",
      templateUrl: "/home.html",
      controller: "myCtrl"
     })

     .state("login",{
      url:"/login",
      templateUrl:"/login.html",
      controller:"loginctrl"

     })

     .state("image",{
      url:"/image",
      templateUrl:"/image.html",
      controller:"imgctrl"
      
      })

     .state("details",{
      url:"details",
      templateUrl:"/details.html",
      controller:"addprodctrl"
     })

     .state("admin",{
      url:"admin",
      templateUrl:"/admin.html",
      controller:"adminctrl"
     })
     .state("aboutus",{
      url:"aboutus",
      templateUrl:"/aboutus.html",
      controller:"aboutus"
      
     })
     .state("order",{
      url:"order",
      templateUrl:"/order.html",
      controller:"orderctrl"

     })
     .state("users",{
      url:"users",
      templateUrl:"/users.html",
      controller:"userctrl"
     })
     .state("vieworder",{
      url:"vieworder",
      templateUrl:"/vieworder.html",
      controller:"vieworderctrl"

     });


 $urlRouterProvider.otherwise('home')
});


  app.controller('myCtrl', function($scope, $http)
   {
    $scope.gender = ["Female","Male","Others"];
    $scope.showme = false;
 
    $scope.submit = function()
      {
        
        $http.post('/useradd',$scope.formdata).
        success(function(data)
        {
          $scope.successmsg=false;
          $scope.errormsg=false;
          if(data.status==true)
          {
            console.log("dtaga",data.status)
            console.log("posted");
            $scope.successmsg=data.message;
           /* $scope.name="";
            $scope.password="";
            $scope.gender="";
            $scope.email="";
            */
            
          }
          else
          {
              console.log("dtaga",data.status)
              $scope.errormsg=data.message;
            
          }
          //location.url('/home/detail.html');
        }).error(function(data)
        {
          console.error("error");
        })
        $scope.msg="user registered"
      }
  });


  app.controller('loginctrl',function($scope,$http,$location)
  {
    console.log("helloooooooo tsk")
    $scope.login=function(logindata)
    {console.log("logindata: ",logindata)
        $http.post('/login',logindata).
        success(function(data)
        {
          console.log("logged in ", data);

        }).error(function(data)
        {
          console.log("not logged in ", data);
        })
      }
});

       
   app.controller('adminctrl',function($scope,$http,$location)
  {
    $scope.loginadmin=function()
    {
    console.log("admindata ssssssss:",admindata);
    
    $http.post('/admin',$scope.admindata).
    success(function(data)
    { 
        console.log("successfully logged in");
        if(data.success)
        {
          $scope.successmsg=data.message;

        }
        else
        {
          $scope.errormsg=data.message;
        }

    }).error(function(data)
      {
      console.log("try again");
       $scope.errormsg=data.message;
      })
  
   }
});
  

 

 /*
app.controller('loginctrl',function($scope, $http,$location)
{
 
  $scope.login=function()
  {
     $http.post('/login',$scope.logindata).
    success(function(data)
    {

      console.log(data);
      var kd = data.session;
      console.log("fff",kd);

      if(data.message=="success")
      {
        $scope.msg=kd;
        console.log("sfafa",kd);
       $location.path('/image');
       window.alert("logged in");
      
      }
      else
      {
        window.alert("not logged in");
      }

         }).error(function(data)
    {
      console.log("failed to log in");

    })
  }

});
*/



/*
app.controller('uploadctrl',function($scope)
{
  scope.upload=function(data)
  {
    $http.post('/upload',$scope).
    success(function(data)
    {
      console.log("uploaded");
    }).error(function(data)
    {
      console.log("error in uploading");
    })

  }
});
app.controller('MyCtrl2', ['$scope', 'Upload', function (Upload,$window) {
    // upload later on form submit or something similar 
    var vm=this;
    var.submit=function()
    {
      if(vm.upload_form.file.$valid && vm.file)
      {
        vm.upload(vm.file);
      }
    }
    vm.upload=function(file)
    {
      Upload.upload(
      {
        Upload.upload({
          url:'http://localhost:3000/upload',
          data:{file:file}
        }).then(function(resp)
        {
          if(resp.data.error_code === 0)
          {
            $window.alert('Success'+resp.config.data.file.name+'uploaded.Response: ');
            console.log(resp.config.data.file.name)
          } 
          else
          {
            $window.alert('an error');
          }
        },function(resp){
          console.log('Error status: '+resp.status);
          $window.alert('Error status: '+resp.status);
        })
      })
    }
    

}]);*/